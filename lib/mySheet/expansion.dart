import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:json_table/json_table.dart';

class expncard1 extends StatefulWidget {
  @override
  _expncardState createState() => _expncardState();
}
DateTime now = DateTime.now();
String formattedDate = DateFormat('dd-MM-yyyy').format(now);

class _expncardState extends State<expncard1> {

  String jsonSample =
      '[{"Check In":"07:00 am","Check out":"09:00 am","Time Worked":"1 h 0 min","Check Out Reason":"Out of service today"},'
      '{"Check In":"07:00 am","Check out":"09:00 am","Time Worked":"1 h 0 min","Check Out Reason":"Break time"},'
      '{"Check In":"07:00 am","Check out":"09:00 am","Time Worked":"1 h 0 min","Check Out Reason":"End"},'
      '{"Check In":"07:00 am","Check out":"09:00 am","Time Worked":"1 h 0 min","Check Out Reason":"Health issue"},'
      '{"Check In":"","Check out":"","Time Worked":"","Check Out Reason":""}]';


  final List<Map<String, String>> listOfColumns = [
    {"Name": "AAAAAA", "Number": "1", "State": "Yes"},
    {"Name": "BBBBBB", "Number": "2", "State": "no"},
    {"Name": "CCCCCC", "Number": "3", "State": "Yes"}
  ];

  DateTime selectedDate = DateTime.now();
  String dob = "date";


  @override
  Widget build(BuildContext context) {

    var json = jsonDecode(jsonSample);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 5),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Select Date",style: new TextStyle(
                  fontSize: 14.0, color: Colors.white),),
              Container(
                height: 20,
                child: FlatButton.icon(

                  color: Colors.white70,
                  icon: Icon(Icons.arrow_drop_down), //`Icon` to display
                  label: Text(' $formattedDate'), //`Text` to display
                  onPressed: () {
                    _selectDate(context);
                    //Code to execute when Floating Action Button is clicked
                    //...
                  },
                ),
              ),

              Container(
                child: Column(
                  children: <Widget>[
                    Text("Total time worked :",style: new TextStyle(
                        fontSize: 14.0, color: Colors.white),),
                    Padding(
                      padding: const EdgeInsets.only(top:8.0),
                      child: Text("3 h 00 min ",style: new TextStyle(
                          fontSize: 14.0, color: Colors.white),),
                    ),
                  ],
                ),
              ),

            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: JsonTable(
              json,
//                    showColumnToggle: true,
              tableHeaderBuilder: (String header) {
                return Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: 6.0, vertical: 6.0),
//                        decoration: BoxDecoration(
//                            border: Border.all(width: 0.5),
//                            color: Colors.grey[300]),
                  child: Text(
                    header,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.display1.copyWith(
//                      fontWeight: FontWeight.w700,
                        fontSize: 14.0,
                        color: Colors.white),
                  ),
                );
              },
              tableCellBuilder: (value) {
                return Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: 4.0, vertical: 1.0),
//                        decoration: BoxDecoration(
//                            border: Border.all(
//                                width: 0.5,
//                                color: Colors.grey.withOpacity(0.5))),
                  child: Text(
                    value,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: 14.0, color: Colors.white),
                  ),
                );
              },
            ),
          ),

              Padding(
                padding: const EdgeInsets.only(left:80.0),
                child: Container(
                  child: Icon(Icons.markunread,color: Colors.white,),
                ),
              ),
          /*Container(
            child: DataTable(
//              onSelectAll: (b) {},
//              sortAscending: true,
              columns: [
                DataColumn(label: Text('Patch',style: TextStyle(color: Colors.white))),
                DataColumn(label: Text('Version',style: TextStyle(color: Colors.white))),
                DataColumn(label: Text('Ready',style: TextStyle(color: Colors.white))),
              ],
              rows:listOfColumns // Loops through dataColumnText, each iteration assigning the value to element
                  .map(
                ((element) => DataRow(
                  cells: <DataCell>[
                    DataCell(Text(element["Name"],style: TextStyle(color: Colors.white),),), //Extracting from Map element the value
                    DataCell(Text(element["Number"],style: TextStyle(color: Colors.white))),
                    DataCell(Text(element["State"],style: TextStyle(color: Colors.white))),
                  ],
                )),
              )
                  .toList(),
            ),
          ),*/

          Container(height: 100,),

      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              RaisedButton(
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                color: Color(0xFF19ACCA),
                onPressed: (){
                },
                child: Text("Job Details"),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                  color: Colors.redAccent,
                  onPressed: (){
                  },
                  child: Text("Confirm"),
                ),
              ),


            ],
          ),

          Text("Contact Employer",style: new TextStyle(
              fontSize: 11.0, color: Colors.white),),
        ],
      ),
        ],
      ),
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        formattedDate ="${selectedDate.day} - ${selectedDate.month} - ${selectedDate.year}";
      });
  }


}
