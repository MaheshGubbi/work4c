import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:work4c/home/thanks.dart';

import 'oprntyCart.dart';


class opportunity extends StatefulWidget {
  @override
  _opportunityState createState() => _opportunityState();
}

class _opportunityState extends State<opportunity> {
  int num =5;
  @override

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

        decoration: new BoxDecoration(
          //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
          gradient: const LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: <Color>[
              const Color(0xFF3F0361),
              const Color(0xFF711D57),
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left:20.0,right: 20,bottom: 10),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: new Border.all(color: Colors.white),
              color: Colors.white
            ),
            child: ListView.builder(
              itemCount: num,
              itemBuilder: (context, index) {
                return Slidable(
                  key: ValueKey(index),
                  actionPane: SlidableDrawerActionPane(),
                  dismissal: SlidableDismissal(
                      child: SlidableDrawerDismissal(),
                      onWillDismiss: (actionType) {
                        return showDialog<bool>(
                          context: context,
                          builder: (context) {
                            return confirm();
                          },
                        );
                      },
                  ),
                  secondaryActions: <Widget>[

                    IconSlideAction(
                      onTap: (){
                        _showDialog();
                      },
                      caption: 'Accept',
                      color: Colors.green,
                      icon: Icons.save,
                    ),
                    IconSlideAction(
                      caption: 'Reject',
                      color: Colors.red,
                      icon: Icons.cancel,
                      onTap: ()=> confirm1()
                    ),
                  ],
//                  dismissal: SlidableDismissal(
//                    child: SlidableDrawerDismissal(),
//                  ),

                  child: opntycard(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  void _showDialog() {

    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          elevation: 0,
          contentPadding: EdgeInsets.all(0.0),
          backgroundColor: Colors.white,
          // title: new Text("$value",style: TextStyle(fontSize: 24.0,color: Colors.purpleAccent),),
          content: Padding(
            padding: const EdgeInsets.only(left: 20,right: 20,top: 20),
            child: Container(
              height: 185,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Container(
                    child: Text("We thank you for your application, once your application is accepted you will see the job in your Check-In section of notifications."
                      ,style: new TextStyle(
                          fontSize: 15.0, color: Colors.black,fontWeight: FontWeight.w500),textAlign: TextAlign.center,),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top:58.0),
                    child: Container(
                      width: 200,
                      child: RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(2.0)),
                        color: Colors.red,
                        elevation: 0,
                        onPressed: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext   context) => new thanks(),
                            ),
                          );
//                          Navigator.pop(context);
                        },
                        child:Text("Close",style: TextStyle(fontSize: 16,color: Colors.white),),
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),


        );
      },
    );
  }
  void confirm1() {

    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text('Reject'),
          content: Text('Confirm ! Are you reject opportunity '),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () => Navigator.of(context).pop(false),
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () => Navigator.of(context).pop(true),
            ),
          ],
        );
      },
    );
  }


  Widget confirm() {
   return( AlertDialog(
      title: Text('Reject'),
      content: Text('Confirm ! Are you reject opportunity '),
      actions: <Widget>[
        FlatButton(
          child: Text('Cancel'),
          onPressed: () => Navigator.of(context).pop(false),
        ),
        FlatButton(
          child: Text('Ok'),
          onPressed: () { Navigator.of(context).pop(true);
          setState(() {
            num = num -1;
          });},
        ),
      ],
    ));
  }



}
