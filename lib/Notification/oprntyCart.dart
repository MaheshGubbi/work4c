import 'package:flutter/material.dart';
import 'package:work4c/filter/jobDetails.dart';

class opntycard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:0.0,right: 8),
      child: Container(
        height: 90,
        child: GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext   context) => new jobdetails(),
              ),
            );
          },
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(
                        context).size.width-161,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Container(height: 5,),
                          Text("Work in site - Construction",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,fontWeight: FontWeight.bold
                          ),),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Text("CSGNY IT Consuluting Pvt Ltd.",style: new TextStyle(
                                fontSize: 11.0,
                                color: Colors.black
                            ),),
                          ),

                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Hourly Rate",style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black ,fontWeight: FontWeight.normal
                                ),),
                                Text("\$10 / \$15",style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black ,fontWeight: FontWeight.bold
                                ),),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 60,width: 1,color: Colors.black,
                  ),
                  Container(
                      width: 100,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[

                            Text("13 july 2019",style: new TextStyle(
                                fontSize: 14.0,
                                color: Colors.black
                            ),),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 5),
                              child: Text("To",style: new TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black
                              ),),
                            ),
                            Text("23 july 2019",style: new TextStyle(
                                fontSize: 14.0,
                                color: Colors.black
                            ),),

                          ],
                        ),
                      )),


                ],
              ),
              Container(
                height: 1,width: MediaQuery.of(context).size.width-100,color: Colors.black26 ,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
