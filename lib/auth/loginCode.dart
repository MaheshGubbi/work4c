import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../main.dart';


class loginCode extends StatefulWidget {
  @override
  _loginCodeState createState() => _loginCodeState();
}

class _loginCodeState extends State<loginCode> {

  bool _obscureText = false;
  final otp = TextEditingController();
  String _number;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              Center(
                child: new Image.asset(
                  'assets/bg.png',
                  width:MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  fit: BoxFit.fill,
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left:65.0,right: 65),
                child: Center(child: Column(
                  children: <Widget>[
                    Container(height:MediaQuery.of(context).size.height*0.3 ,),
                    new Image.asset(
                      'assets/logo.png',
                      width:100,
                      height:100,
                      fit: BoxFit.fill,
                    ),

                    Center(
                      child: Padding(
                        padding: const EdgeInsets.only(top:15,bottom: 15 ,left: 8,right: 8),
                        child: Text("We have sent you the access code"
                            " via SMS for mobile number"
                            " verification",
                          style: TextStyle(fontSize: 16,color: Colors.white70,),textAlign: TextAlign.center,),
                      ),
                    ),
                    TextFormField(
                      maxLength: 4,
                      textAlign: TextAlign.center,
                      key: Key('otp'),
                      keyboardType: TextInputType.number,
                      obscureText: !_obscureText? true: false,
                      decoration: InputDecoration(
                        counterText: '',
                        counterStyle: TextStyle(fontSize: 0),
                        filled: true,
                        border: InputBorder.none,
                        fillColor: Colors.white,
                        hintText: 'XXXX',
                        hintStyle: TextStyle(fontSize: 12, color: Colors.black54, ),
                      ),
                      validator: validateotp

                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top:8.0,bottom: 8),
                          child: Text("Did't receive a SMS ? ",style: TextStyle(color: Color(0xFFFCFCFC)),),
                        ),
                        Text("Resend ",style: TextStyle(color: Colors.red),),
                      ],
                    ),


                    Padding(
                      padding: const EdgeInsets.only(top:48.0),
                      child: GestureDetector(
                        onTap: (){
                          _formKey.currentState.validate()?
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext   context) => new HomePage(tab: 0,follow : 1),
                            ),
                          ):
                          Fluttertoast.showToast(msg: "Enter OTP ");

                        },
                        child: Container(
                          height: 50,
                          decoration: new BoxDecoration(
                            //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
                            gradient: const LinearGradient(
                              begin: FractionalOffset.centerRight,
                              end: FractionalOffset.centerLeft,
                              colors: <Color>[
                                const Color(0xFFFF473A),
                                const Color(0xFFFF473A),
                              ],
                            ),
                          ),
                          child: Center(
                            child: Text("Done",style: TextStyle(
                                fontSize: 16 ,fontWeight: FontWeight.bold,
                                color: Color(0xFFFFFFFF))),
                          ),


                        ),
                      ),
                    ),


                  ],
                )),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String validateotp(String value) {
    if (value.length < 4)
      return 'Enter OTP';
    else
      return null;
  }

  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }
  }
}
