import 'package:flutter/material.dart';

import '../main.dart';
import 'loginCode.dart';
import 'signUp.dart';

class login extends StatelessWidget {
  bool _obscureText = false;
  String _password;
  String _number;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Center(
              child: new Image.asset(
                'assets/bg.png',
                width:MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                fit: BoxFit.fill,
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(left:65.0,right: 65),
              child: Center(child: Column(
                children: <Widget>[

                  Container(height:MediaQuery.of(context).size.height*0.3 ,),
                  new Image.asset(
                    'assets/logo.png',
                    width:100,
                    height:100,
                    fit: BoxFit.fill,
                  ),
                  Container(height: 42,),
                  /*new TextFormField(
                   // key: Key('number'),
                    maxLength: 10,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      counterText: '',
                      counterStyle: TextStyle(fontSize: 0),
                      prefixIcon: Container(
                        height: 6,
                        color: Colors.black,
                        child: Center(
                          widthFactor: 0.0,
                          child: Text('+91', style: TextStyle(color:Colors.white, )),
                        ),
                      ),
//                      prefixText: "+91 ",
//                      prefixStyle: new TextStyle(decorationColor: Colors.purple,
//                        color: Colors.black,fontWeight: FontWeight.bold
//                      ),
                      filled: true,
                      fillColor: Colors.white,
//                  labelText: 'Mobile Number',
                      hintText: 'Mobile Number',
                      hintStyle: TextStyle(fontSize: 16,fontWeight: FontWeight.w300, color: Colors.black),
                      //contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                      border: OutlineInputBorder(
                          borderSide: new BorderSide(color: Colors.redAccent),
                          borderRadius: BorderRadius.circular(8.0)),

                    ),
//                validator: EmailFieldValidator.validate,

                    onSaved: (value) => _number = value,
                  ),*/



                  TextFormField(
                    style: TextStyle(color: Colors.black,fontSize: 16),
                    keyboardType: TextInputType.number,
                    maxLength: 10,
                    decoration: InputDecoration(
                        counterText: '',
                        counterStyle: TextStyle(fontSize: 0),
                      filled: true,
                      fillColor: Colors.white,
                      prefixIcon: Container(
                        height: 4,
                        color: Colors.black,
                        child: Center(
                          widthFactor: 0.0,
                          child: Text('+91', style: TextStyle(color:Colors.white, )),
                        ),
                      ),
                        border: InputBorder.none,
//                      border: new UnderlineInputBorder(
//                          borderSide: new BorderSide(
//                              color: Colors.red
//                          )
//                      ),
                      hintText: 'Mobile Number',
                        hintStyle: TextStyle(fontSize: 12,fontWeight: FontWeight.w300, color: Colors.black)
                    ),
//                    controller: _usernameController,
                  ),

                  Container(height: 2,width: 300,),

                  new TextFormField(
                    style: TextStyle(color: Colors.black,fontSize: 14),
                    maxLength: 10,
                    key: Key('password'),
                    keyboardType: TextInputType.text,
                    obscureText: !_obscureText? true: false,
                    decoration: InputDecoration(
                      counterText: '',
                      counterStyle: TextStyle(fontSize: 0),
                      filled: true,
                      fillColor: Colors.white,
//                  labelText: 'Password',
                      hintText: 'Password',
                      hintStyle: TextStyle(fontSize: 12,fontWeight: FontWeight.w300, color: Colors.black),
                      border: InputBorder.none,
//                      border: OutlineInputBorder(
//                          borderSide: new BorderSide(color: Colors.redAccent),
//                          borderRadius: BorderRadius.circular(0.0)),

                      suffixIcon: GestureDetector(
                        onTap: () {
                            _obscureText = !_obscureText;

                        },
                        child: Icon(
                          _obscureText ? Icons.visibility : Icons.visibility_off,color:Colors.redAccent,
                          semanticLabel: _obscureText ? 'show password' : 'hide password',
                        ),
                      ),
                    ),
                    validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
                    //validator: PasswordFieldValidator.validate,
                    onSaved: (value) => _password = value,
                    //border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),icon: Icon(Icons.search)),
                  ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("Forget your password ?",style: TextStyle(color: Color(0xFFFCFCFC)),),
                      ),
                    ],
                  ),
                  Padding(padding:EdgeInsets.symmetric(vertical: 10)),
                  SizedBox(
                    width: double.infinity,
                    height: 45,
                    child: RaisedButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(0.0)),
                      color: Colors.red,
                      elevation: 0,
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext   context) => new loginCode(),
                          ),
                        );
                      },
                      child:Text("Next",style: TextStyle(fontSize: 14,color: Colors.white),),
                    ),
                  ),

                  Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext   context) => new singup(),
                            ),
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Don't have an account ?",style: TextStyle(color: Color(0xFFFCFCFC)),),
                        ),
                      ),
                    ],
                  ),


                ],
              )),
            ),
          ],
        ),
      ),
    );
  }
}
