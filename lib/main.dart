

import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:work4c/sidebar/sidebar.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'auth/login.dart';
import 'filter/workIn.dart';
import 'home/CheckIn.dart';
import 'home/companies.dart';
import 'home/jobs.dart';
import 'home/notifnIcon.dart';
import 'sidebar/notification.dart';


void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.red,
        hintColor: Colors.white,
        fontFamily: 'Montserrat',

      ),
      home: new HomePage(tab: 0,follow : 1),
    );
  }
}

class HomePage extends StatefulWidget {
  final int tab;
  final int follow;
  HomePage ({Key key,this.tab,this.follow}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin{

  bool changep = true;
  String _title="";
  int _page = 0;
  final List<Tab> tabs = <Tab>[
    new Tab(text: "       Jobs        "),
    new Tab(text: "   Companies  "),
  ];
  DateTime currentBackPressTime;

  TabController _tabController;

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Please click BACK again to exit");
      return Future.value(false);
    }
    return Future.value(true);
  }
  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ?? false;
  }


  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: tabs.length,initialIndex: widget.tab);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,// _onWillPop,
      child: new Scaffold(
        appBar:
        new AppBar(
          backgroundColor: Color(0xFF3F0361),
          elevation: 0,
          actions: <Widget>[
            ntfnIcon(),

//            onPressed: (){
//              Navigator.push(
//                context,
//                MaterialPageRoute(
//                  builder: (BuildContext   context) => new login(),
//                ),
//              );

          ],
          centerTitle: true,
          flexibleSpace: SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(top:10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 33,
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(50.0),
                      color: Color(0xFF4AFFFFFF),
                    ),
                    child: new TabBar(
                      isScrollable: true,
                      unselectedLabelColor: Colors.white,
                      labelColor: Colors.black,
                      indicatorSize: TabBarIndicatorSize.tab,
                      indicator: new BubbleTabIndicator(
                        indicatorHeight: 30.0,
                        indicatorColor: Colors.white,
                        tabBarIndicatorSize: TabBarIndicatorSize.tab,
                      ),
                      tabs: tabs,
                      controller: _tabController,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
//            : AppBar(
//        title: Text(_title),
//        bottom: new TabBar(
//          tabs: <Widget>[
//            new Tab(
//              text: "First",
//            ),
//            new Tab(
//              text: "Second",
//            ),
//          ],
//        ),
//      ),
        drawer: MyDrawer(),
//      new Drawer(
//        child: Container(
//          color: Colors.black,
//          child: new ListView(
//            children: <Widget>[
//              Padding(
//                padding: const EdgeInsets.only(left:20.0,top: 25),
//                child: Row(
//                  children: <Widget>[
//                     ClipOval(
//                        child:Image.asset("assets/dp.png",height: 100,
//                            width: 100,fit: BoxFit.fill,)
//                      ),
//
////                child: new UserAccountsDrawerHeader(
////                  accountEmail: new Text("thetechnowb@gmail.com"),
////                  accountName: new Text("Tarun Joshi"),
////                  currentAccountPicture: new CircleAvatar(
////                    backgroundColor: Colors.white,
////                    child: new Text("T"),
////                  ),
//////                  otherAccountsPictures: <Widget>[
//////                    new CircleAvatar(
//////                      backgroundColor: Colors.white,
//////                      child: new Text("J"),
//////                    ),
//////                    new CircleAvatar(
//////                      backgroundColor: Colors.white,
//////                      child: new Text("P"),
//////                    ),
//////                  ],
////                ),
//
//                    Padding(
//                      padding: const EdgeInsets.only(left:15.0),
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        children: <Widget>[
//                          Row(
//                            children: <Widget>[
//                              new Text("Musa Ahmed",style: TextStyle(color: Colors.white),),
//                              Padding(padding: EdgeInsets.only(left: 10)),
//                              new Icon(Icons.edit,color: Colors.white,)
//                            ],
//                          ),
//                          new Text("musa@zamask.com",style: TextStyle(color: Colors.white),),
//                          new Text("+91 - 999999999",style: TextStyle(color: Colors.white),),
//                        ],
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//              Container(
//                height: 100,
//              ),
//              new ListTile(
//                onTap: (){
//                  setState(() {
//                    changep =true;
//                    Navigator.pop(context);
//
//                  });
//                },
//                title: Row(
//                  children: <Widget>[
//                    new Icon(Icons.notifications_none,color: Colors.white),
//                    Padding(padding: EdgeInsets.only(left: 8)),
//                    new Text("DashBoadr",style: TextStyle(color: Colors.white),),
//                  ],
//                ),
//              ),
//              new ListTile(
//                onTap: (){
//                  setState(() {
//                    _page = 1;
//                    changep =false;
//                    _title ="Notification";
//                    Navigator.pop(context);
//
//                  });
//                },
//                title: Row(
//                  children: <Widget>[
//                    new Icon(Icons.notifications_none,color: Colors.white),
//                    Padding(padding: EdgeInsets.only(left: 8)),
//                    new Text("My Notification",style: TextStyle(color: Colors.white),),
//                  ],
//                ),
//              ),
//              new ListTile(
//                onTap: (){
//                  setState(() {
//                    changep =false;
//                    _title ="My Timesheets";
//                    Navigator.pop(context);
//
//                  });
//                },
//                title: Row(
//                  children: <Widget>[
//                    new Icon(Icons.library_books,color: Colors.white,),
//                    Padding(padding: EdgeInsets.only(left: 8)),
//                    new Text("My Timesheets",style: TextStyle(color: Colors.white),),
//                  ],
//                ),
//              ),
//              new Divider(height: 5.0),
//              new ListTile(
//                onTap: (){
//                  setState(() {
//                    changep =false;
//                    _title ="MY Jobs";
//                    Navigator.pop(context);
//
//                  });
//                },
//                title: Row(
//                  children: <Widget>[
//                    new Icon(Icons.content_paste,color: Colors.white),
//                    Padding(padding: EdgeInsets.only(left: 8)),
//                    new Text("MY Jobs (9)",style: TextStyle(color: Colors.white),),
//                  ],
//                ),
//
//              ),
//              new ListTile(
//                onTap: (){
//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(
//                        builder: (BuildContext   context) => new checkIn(),
//                    ),
//                  );
//                },
//                title: Row(
//                  children: <Widget>[
//                    new Icon(Icons.favorite_border,color: Colors.white),
//                    Padding(padding: EdgeInsets.only(left: 8)),
//                    new Text("Company I Follow (2)",style: TextStyle(color: Colors.white),),
//                  ],
//                ),
//
//              )
//            ],
//          ),
//        ),
//      ),
        body:  //Page(),


        Container(
          child: new TabBarView(
            children: <Widget>[
              new jobs(),
              new company(follow :widget.follow),
            ],
            controller: _tabController,
//        controller: _tabController,
//        children: tabs.map((Tab tab) {
//          print(tab);
//          return new Container(
//            child:tab == "Companies" ? jobs() :company(),
//          );
////          Center(
////
////              child:  new RaisedButton(
////                child: new Text('Play!$tab'),
////                color: Colors.blueAccent,
////              ),
////          );
//        }).toList(),
          ),
          //Page(_page),
        ),
      ),
    );
  }


}



