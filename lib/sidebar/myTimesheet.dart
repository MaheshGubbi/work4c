import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:work4c/Notification/myJobs.dart';
import 'package:work4c/Notification/opportunity.dart';
import 'package:work4c/home/companies.dart';
import 'package:work4c/home/jobs.dart';
import 'package:work4c/home/notifnIcon.dart';
import 'package:work4c/mySheet/expansion.dart';
import 'package:work4c/sidebar/sidebar.dart';

const month_report = ["Apr 2019", "May 2019", "Jun 2019", "Jul 2019"];

class mySheet extends StatefulWidget {
  @override
  _notificationState createState() => _notificationState();
}

class _notificationState extends State<mySheet> with SingleTickerProviderStateMixin{
  DateTime selectedDate = DateTime.now();
  String dob = "Date of birth";
  var curPos = 0;
  void _update() {
    setState(() {});
//    widget.onClick(curPos);
  }

  void _next() {
    curPos++;
    if (curPos >= month_report.length) {
      curPos = 0;
    }
    _update();
  }

  void _back() {
    curPos--;
    if (curPos < month_report.length) {
      curPos = 0;
    }
    _update();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar://changep?
      new AppBar(
        backgroundColor: Color(0xFF3F0361),
        title: Text("MY Timesheets"),
        actions: <Widget>[
          IconButton(icon: new Icon(Icons.close),
            onPressed: (){
              Navigator.pop(context);
              // _showMaterialSearch(context);
            },
          ),
        ],
        centerTitle: true,
      ),
      drawer: MyDrawer(),

      body:
      Container(
        color: Color(0xFF3F0361),
        child: Column(
          children: <Widget>[
                        Container(
              child:  Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                            Icons.keyboard_arrow_left,
                            color: Colors.white
                        ),
                        onPressed: _back,
                      ),
                      Text(
                        "${month_report[curPos]}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                            color: Colors.white, fontSize: 16
                        ),
                      ),
                      IconButton(
                        icon: Icon(
                            Icons.keyboard_arrow_right,
                            color: Colors.white
                        ),
                        onPressed: _next,
                      ),
                    ],
                  ),
                ],
              ),
              //Page(_page),
            ),

            
            Expanded(
              child: new ListView.builder(
                itemCount: vehicles.length,
                itemBuilder: (context, i) {
                  return  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 3 ),
                    child: Container(  //Card(
                      color: Color(0xFF333333),
                      child: new ExpansionTile(

                        title:titlecard(),
//                    new Text(vehicles[i].title,
//                      style: new TextStyle(color: Colors.white,fontSize: 20.0, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),),
                        children: <Widget>[
                         expncard1(),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),




          ],
        ),
      ),
    );
  }

  _buildExpandableContent(Vehicle vehicle) {
    List<Widget> columnContent = [];

    for (String content in vehicle.contents)
      columnContent.add(
        new ListTile(
          title: new Text(content, style: new TextStyle(fontSize: 18.0),),
          leading: new Icon(vehicle.icon),
        ),
      );

    return columnContent;
  }

  titlecard() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(width: 25,height: 80,
            color: Colors.red,),
          ],
        ),
        Row(
          children: <Widget>[
            Container(

                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Work in site - Construction",style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.white,fontWeight: FontWeight.bold
                      ),),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text("CSGNY IT Consuluting Pvt Ltd.",style: new TextStyle(
                            fontSize: 11.0,
                            color: Colors.white
                        ),),
                      ),


                    ],
                  ),
                )),
          ],
        ),

        Container(
            width: 100,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[

                  Text("13 july 2019",style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.white
                  ),),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: Text("To",style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.white
                    ),),
                  ),
                  Text("23 july 2019",style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.white
                  ),),

                ],
              ),
            )),


      ],
    );
  }

  expncard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text("Select Date",style: new TextStyle(
                  fontSize: 14.0, color: Colors.white),),
              GestureDetector(
                onTap: (){
                  _selectDate(context);
                },
                child: Container(
                  height: 45,
                  child: Row(
                    children: <Widget>[
                      Text("$dob",style: TextStyle(fontSize: 14, color: Colors.white),),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dob ="${selectedDate.day} - ${selectedDate.month} - ${selectedDate.year}";
      });
  }

}

class Vehicle {
  final String title;
  List<String> contents = [];
  final IconData icon;

  Vehicle(this.title, this.contents, this.icon);
}

List<Vehicle> vehicles = [
  new Vehicle(
    'Bike',
    ['Vehicle no. 1', 'Vehicle no. 2', 'Vehicle no. 7', 'Vehicle no. 10'],
    Icons.motorcycle,
  ),
  new Vehicle(
    'Cars',
    ['Vehicle no. 3', 'Vehicle no. 4', 'Vehicle no. 6'],
    Icons.directions_car,
  ),
];
