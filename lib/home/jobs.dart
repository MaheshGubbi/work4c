import 'package:flutter/material.dart';
import 'package:work4c/filter/jobFilter.dart';

import 'jobCard.dart';

class jobs extends StatefulWidget {
  @override
  _jobsState createState() => _jobsState();
}

class _jobsState extends State<jobs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton:  Padding(
        padding: const EdgeInsets.only(bottom:50.0),
        child: FloatingActionButton(
          backgroundColor: Colors.redAccent,
          elevation: 0,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext   context) => new jobFilter(),
              ),
            );          },
          child: Icon(Icons.filter_list,size: 30,),
          mini: true,
//          shape: RoundedRectangleBorder(),
          // heroTag: "demoTag",
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: new BoxDecoration(
          //shape:BoxShape.circle ,
//                  borderRadius: BorderRadius.circular(30.0),
          gradient: const LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: <Color>[
              const Color(0xFF3F0361),
              const Color(0xFF711D57),
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left:26.0,right: 26),
         child: ListView(
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.only(top:8.0,bottom: 20),
                child: Container(
                  height: 35,
                  decoration: new BoxDecoration(
//                    shape: BoxShape.circle,// You can use like this way or like the below line
                    borderRadius: new BorderRadius.circular(30.0),
                    color: Color(0xFF4AFFFFFF),
//                    border: Border.all(color: Colors.white)
                  ),
                  child: TextField(
                   style: new TextStyle(
                       fontSize: 18.0,
                       color: Colors.white,fontWeight: FontWeight.w500
                   ),
//            controller: editingController,
                    decoration: InputDecoration(
//                labelText: "Search",
                        hintText: "Search Jobs",
                        hintStyle: TextStyle(fontSize: 12,fontWeight: FontWeight.w500, color: Colors.white),
                        prefixIcon: Icon(Icons.search,color: Colors.white,),
                        border: InputBorder.none,
                    ),
                  ),
                ),
              ),

                  jobcard(),
                  jobcard(),
                  jobcard(),
                  jobcard(),
                  jobcard(),
                  jobcard(),
                  jobcard(),

//              SingleChildScrollView(
//                  child:
//                  Container(
//                    child: Column(
//                      children: <Widget>[
//                        jobcard(),
//                        jobcard(),
//                        jobcard(),
//                        jobcard(),
//                        jobcard(),
//                        jobcard(),
//                      ],
//                    ),
//                  )
//              ),
            ],
          ),
        ),
      ),
    );
  }
}
