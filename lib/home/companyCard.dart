import 'package:flutter/material.dart';
import 'package:work4c/filter/cmpDetails.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class companycard extends StatefulWidget {
  @override
  _companycardState createState() => _companycardState();
}

class _companycardState extends State<companycard> {

  bool flw = true;
  bool flw1 = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    flw = true;
    flw1 = true;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext   context) => new cmpDetails(),
              ),
            );

          },
          child: Container(
            decoration: new BoxDecoration(
              borderRadius: new BorderRadius.circular(10.0),
              color: Color(0xFF00395A),
            ),

          child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text("CSGNY IT Consuluting Pvt Ltd. ",style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,fontWeight: FontWeight.bold
                      ),),

                      Padding(
                        padding: const EdgeInsets.only(top:8.0,bottom: 8),
                        child: Container(height: 20,
                          child: flw ? OutlineButton(
                            borderSide: BorderSide(
                              color: Colors.white, //Color of the border
                              style: BorderStyle.solid, //Style of the border
                              width: 0.8, //width of the border
                            ),
                            color: Colors.redAccent,
                            onPressed: (){
                              setState(() {
                                flw=!flw;
                              });
                            },
                            child: Text("Following",style: TextStyle(color: Colors.white),),

                          ): RaisedButton(
                            color: Colors.redAccent,
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                            onPressed: (){
                              setState(() {
                                flw=!flw;
                              });
                            },
                            child: Text("Follow"),

                          ),
                        ),
                      ),
                      Text("Jobs   10",style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,
                      ),),
                    ],
                  ),
                ),

                Container(
                  decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                    color: Colors.white,
                  ),

                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                             Icon(Icons.web,color: Colors.redAccent,size: 30,),
                              Text("  csgitconsulting.com",style: new TextStyle(
                                  fontSize: 13.0,
                                  color: Colors.black
                              ),),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                             Icon(Icons.location_on,color: Colors.redAccent,size: 30,),
                              Text("  San Francisco. CA",style: new TextStyle(
                                  fontSize: 13.0,
                                  color: Colors.black
                              ),),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(height: 5,),
        GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext   context) => new cmpDetails(),
              ),
            );

          },
          child: Container(
            decoration: new BoxDecoration(
              borderRadius: new BorderRadius.circular(10.0),
              color: Color(0xFF00395A),
            ),

          child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text("CSGNY IT Consuluting Pvt Ltd. ",style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,fontWeight: FontWeight.bold
                      ),),

                      Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Container(height: 20,
                          child: flw1 ?RaisedButton(
                            color: Colors.redAccent,
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                            onPressed: (){
                              setState(() {
                                flw1=!flw1;
                              });
                            },
                            child: Text("Follow"),

                          ) :
                          OutlineButton(
                            borderSide: BorderSide(
                              color: Colors.white, //Color of the border
                              style: BorderStyle.solid, //Style of the border
                              width: 0.8, //width of the border
                            ),
                            color: Colors.redAccent,
                            onPressed: (){
                              setState(() {
                                flw1=!flw1;
                              });
                            },
                            child: Text("Following",style: TextStyle(color: Colors.white),),

                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Text("Jobs   10",style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.white,
                        ),),
                      ),
                    ],
                  ),
                ),

                Container(
                  decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                    color: Colors.white,
                  ),

                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                             Icon(Icons.web,color: Colors.redAccent,size: 30,),
                              Text("  csgitconsulting.com",style: new TextStyle(
                                  fontSize: 13.0,
                                  color: Colors.black
                              ),),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                             Icon(Icons.location_on,color: Colors.redAccent,size: 30,),
                              Text("  San Francisco. CA",style: new TextStyle(
                                  fontSize: 13.0,
                                  color: Colors.black
                              ),),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),

//Container(height: 5,),
        /*GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext   context) => new cmpDetails(),
              ),
            );

          },
          child: Column(
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Container(
                  height: 62,
                  decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
                    color: Color(0xFF00395A),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        Text("Zamask Technologies pvt Ltd ",style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                        ),),

                        Padding(
                          padding: const EdgeInsets.only(top:8.0),
                          child: Container(height: 20,
                            child: flw1 ?RaisedButton(
                              color: Colors.redAccent,
                              onPressed: (){
                                setState(() {
                                  flw1=!flw1;
                                });
                              },
                              child: Text("Follow"),

                            ) :
                            OutlineButton(
                              borderSide: BorderSide(
                                color: Colors.white, //Color of the border
                                style: BorderStyle.solid, //Style of the border
                                width: 0.8, //width of the border
                              ),
                              color: Colors.redAccent,
                              onPressed: (){
                                setState(() {
                                  flw1=!flw1;
                                });
                              },
                              child: Text("Following",style: TextStyle(color: Colors.white),),

                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),

              Container(
                decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                  color: Colors.white,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.web,color: Colors.redAccent,),
                                Text("  csgitconsulting.com",style: new TextStyle(
                                    fontSize: 11.0,
                                    color: Colors.black
                                ),),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.location_on,color: Colors.redAccent,),
                                Text("  San Francisco. CA",style: new TextStyle(
                                    fontSize: 11.0,
                                    color: Colors.black
                                ),),
                              ],
                            ),
                          ],
                        ),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Jobs",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black ,fontWeight: FontWeight.normal
                          ),),
                          Text("Ratings",style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black ,fontWeight: FontWeight.normal
                          ),),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:6.0,bottom: 6),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text("10",style: new TextStyle(
                                fontSize: 14.0,
                                color: Colors.black ,fontWeight: FontWeight.normal
                            ),),
                            FlutterRatingBar(
                              initialRating: 4.5,
                              itemSize: 20.0,
                              fillColor: Colors.amber,
                              borderColor: Colors.amber.withAlpha(100),
                              allowHalfRating: true,
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:30.0),
                        child: Row(
                          children: <Widget>[
                            Text("Work Types: Construction, Carpenters ",style: new TextStyle(
                                fontSize: 11.0,
                                color: Colors.black ,fontWeight: FontWeight.normal
                            ),),
                          ],
                        ),
                      ),

                    ],
                  ),
                ),
              ),
            ],
          ),
        ),*/
        Container(height: 5,),
      ],
    );
  }

   follow() {
    RaisedButton(
      color: Colors.redAccent,
        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(50.0)),
      onPressed: (){
        setState(() {
          flw=!flw;
        });
      },
      child: Text("Follow"),

    );
  }

  following() {
    OutlineButton(
      borderSide: BorderSide(
        color: Colors.white, //Color of the border
        style: BorderStyle.solid, //Style of the border
        width: 0.8, //width of the border
      ),
      color: Colors.redAccent,
      onPressed: (){
        setState(() {
          flw=!flw;
        });
      },
      child: Text("Following",style: TextStyle(color: Colors.white),),

    );
  }
}
