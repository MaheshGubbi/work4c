import 'package:flutter/material.dart';
import 'package:work4c/filter/jobDetails.dart';
class jobcard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 123,
      child: GestureDetector(
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => new jobdetails(),
            ),
          );
        },
        child: Card(
          child: Row(
            children: <Widget>[
               Container(
                 width: 100,
                   child: Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Column(
                       children: <Widget>[
                         Text("Hourly rate",style: new TextStyle(
                             fontSize: 14.0,color: Colors.black
                         ),),
                         Padding(
                           padding: const EdgeInsets.only(top:5.0,bottom: 6),
                           child: Text("\$10 / \$14",style: new TextStyle(
                               fontSize: 18.0,
                               fontFamily: 'Roboto',
                               color: Colors.black ,fontWeight: FontWeight.bold
                           ),),
                         ),
                         Text("13 july 2019",style: new TextStyle(
                             fontSize: 14.0,
                             fontFamily: 'Roboto',
                             color: Colors.black
                         ),),
                         Padding(
                           padding: const EdgeInsets.all(1.0),
                           child: Text("To",style: new TextStyle(
                               fontSize: 14.0,
                               fontFamily: 'Roboto',
                               color: Colors.black
                           ),),
                         ),
                         Text("23 july 2019",style: new TextStyle(
                             fontSize: 14.0,
                             fontFamily: 'Roboto',
                             color: Colors.black
                         ),),

                       ],
                     ),
                   )),
              Container(
                height: 100,width: 1,color: Color(0xFF004F68),
              ),

               Container(
                   width: MediaQuery.of(context).size.width -161,
                   child: Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Column(
                       children: <Widget>[
                         Text("Work in site - Construction",style: new TextStyle(
                           fontSize: 12.0,
                             fontStyle: FontStyle.normal,
                             fontFamily: 'Mblack',
                           color: Colors.black,fontWeight: FontWeight.bold
                         ),),
                         Text("CSGNY IT Consuluting Pvt Ltd.",style: new TextStyle(
                             fontSize: 11.0,
                             fontStyle: FontStyle.normal,
                             fontFamily: 'Montserrat',
                             color: Colors.black
                         ),),
                         Padding(
                           padding: const EdgeInsets.only(top:6.0,bottom: 6),
                           child: Row(
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Icon(Icons.location_on,color: Colors.red,),
                               Text(" San Francisco. CA ",style: new TextStyle(
                                   fontSize: 14.0,
                                   color: Color(0xFF4F4F4F) ,fontWeight: FontWeight.normal
                               ),),
                             ],
                           ),
                         ),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                           children: <Widget>[
                             Text("Required",style: new TextStyle(
                                 fontSize: 14.0,
                                 color: Colors.black ,fontWeight: FontWeight.w500
                             ),),

                           ],
                         ),
                         Padding(padding: EdgeInsetsDirectional.only(bottom: 2)),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                           children: <Widget>[
                             Text("20",style: new TextStyle(
                                 fontSize: 14.0,
                                 fontFamily: 'Roboto',
                                 color: Colors.black ,fontWeight: FontWeight.bold
                             ),),
                           ],
                         ),


                       ],
                     ),
                   )),
            ],
          ),
        ),
      ),
    );
  }
}
